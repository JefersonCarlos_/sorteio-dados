package com.company;

import java.util.Random;

public class Sorteador {

    private int resultado;

    public void somaResultado(int valorSorteado) {
       this.resultado += valorSorteado;
    }

    public void sortearNumero() {
        Dado dado = new Dado();
        IO io = new IO();
        Random random = new Random();

        dado.setQtd_faces(io.informeQtdeFace());
        if (dado.getQtd_faces() != 0) {
            for (int i = 1; i <= 3; i++) {
                this.resultado = 0;
                for (int j = 1; j <= 3; j++) {
                    int valorSorteado = (1 + random.nextInt(dado.getQtd_faces()));
                    somaResultado(valorSorteado);
                    io.exibeSaida("Numero Sorteado: " + valorSorteado);
                }
                io.exibeSaida("A soma dos numeros sorteados é: " + this.resultado);
            }
        }
    }
}
