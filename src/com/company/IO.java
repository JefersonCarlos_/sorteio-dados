package com.company;

import java.util.Scanner;

public class IO {

    public void exibeSaida(String mensagem) {
        System.out.println(mensagem);
    }

    public int informeQtdeFace() {
        exibeSaida("Informe a quantidade de faces a serem sorteadas:");
        Scanner scanner = new Scanner(System.in);
        try {
            int qtde_faces = scanner.nextInt();
            return qtde_faces;

        } catch (Exception e) {
            exibeSaida("Leitura incorreta -informe um número inteiro!");
            return 0;
        }
    }
}
